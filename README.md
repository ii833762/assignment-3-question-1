# Introduction to Numerical Modelling
# Assignment 3
# Question 1

import numpy as np
import matplotlib.pyplot as plt

print ()
# Question 1
print ("Question 1: Evaluate the wind from the pressure gradient using \
second order finite difference formulae.")
print ()

# Define the constants
pa = 1e5            # mean atmospheric pressure (Pa)
pb = 200            # pressure variations (Pa)
L = 2.4e6           # length scale of variations (m)
f = 1e-4            # coriolis parameter (rad/s)
r = 1               # density of air (kg/m^3)
ymin = 0            # y domain start (m)
ymax = 1e6          # y domain end (m)

# Size and resolution of the domain
N = 10                        # number of intervals
dy = (ymax - ymin)/N          # size of intervals (m)

# Give the domain a dimension y
y = np.linspace(ymin, ymax, N+1)

# Pressure across the domain
p = pa + pb*np.cos(y*np.pi/L)

# Pressure gradient across the domain
dpdy = -pb*(np.pi/L)*np.sin(y*np.pi/L)

# Geostrophic wind across the y domain
geowind = (-1/(r*f))*dpdy

# Numerical wind across the domain
# inital array of zeroes
numwind = np.zeros_like(y)
# wind for N=0 - 1st order
numwind[0] = (-1/(r*f))*(p[1]-p[0])/dy
# wind for N=N - 1st order
numwind[N] = (-1/(r*f))*(p[N]-p[N-1])/dy
# wind for 0<N<N - 2nd order
for i in range(1,N):
    numwind[i] = (-1/(r*f))*(p[i+1]-p[i-1])/(2*dy)

# Plot geostrophic and numerical wind over the domain
plt.plot(y/1000, geowind, label="geostrophic")
plt.plot(y/1000, numwind, label="numerical")
plt.legend()
plt.title("Geostrophic wind vs 2nd and 1st order numerical wind")
plt.xlabel("y (km)")
plt.ylabel("wind speed (m/s)")
plt.xlim(0, 1000)
plt.ylim(0,2.6)
plt.tight_layout()
plt.show()

# Plot the errors in the numerical solution
plt.plot(y/1000, numwind-geowind)
plt.title("Error in 2nd and 1st order numerical wind vs geostrophic wind")
plt.xlabel("y (km)")
plt.ylabel("error in numerical wind speed (m/s)")
plt.xlim(0,1000)
plt.ylim(-0.1,0.2)
plt.tight_layout()
plt.show()

# Plot the errors in the numerical solution - crop out 1st order section
plt.plot(y/1000, numwind-geowind)
plt.title("Error in 2nd order numerical wind vs geostrophic wind")
plt.xlabel("y (km)")
plt.ylabel("error in numerical wind speed (m/s)")
plt.xlim(0,1000)
plt.ylim(-0.02,0.02)
plt.tight_layout()
plt.show()

# Testing ideas

# index error
# print (numwind[12])

# asserion error
# assert numwind[2] > numwind[8]